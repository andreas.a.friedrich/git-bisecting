
# Purpose 
The aim of this repository is to teach the usefulness of the git bisect command.

There is a single picture in app.zip. It's a picture of leaves only in the first few commits, but at some point a bug (ladybug) pops up. However we don't know when.

The task for the audience is to find the first commit where the bug pops up. The picture is zipped to prevent cheating via git tools (and to emulate work that needs to be done to reproduce the bug).

## Steps:

```
git clone https://gitlab.com/andreas.a.friedrich/git-bisecting
git bisect start
git bisect good 72a3929ef3317d99fa7b3f912f71d6df1282ab25 # the initial commit
git bisect bad master
```
Unzip app.zip and have a look at the picture.

Picture has no bug:
```
git bisect good
```
Picture has a bug:
```
git bisect bad
```

Repeat the above until git tells you the first bad commit.
Return to normal branch via
```
git bisect reset
```
